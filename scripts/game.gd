extends Node2D

@onready var level_generator = $LevelGenerator
@onready var ground_sprite = $GroundSprite
@onready var parallazx1 = $ParallaxBackground/ParallaxLayer
@onready var parallazx2 = $ParallaxBackground/ParallaxLayer2
@onready var parallazx3 = $ParallaxBackground/ParallaxLayer3
@onready var hud = $UILayer/HUD


@export var player_spown_pos_y_offset: float = 135

var viewport_size: Vector2
var player: Player = null
var player_spown_pos: Vector2
var player_scene = preload("res://scenes/player.tscn")
var camera_scene = preload("res://scenes/game_camera.tscn")
var camera = null
var score: int = 0
var high_score: int = 0
var save_file_path = "user://highscore.save"
var new_skin_unlocked = false

signal player_died(scroe, highscore)
signal pause_game

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	viewport_size = get_viewport_rect().size
	hud.visible = false
	player_spown_pos.x = viewport_size.x / 2
	player_spown_pos.y = viewport_size.y - player_spown_pos_y_offset
	ground_sprite.global_position.x = viewport_size.x / 2
	ground_sprite.global_position.y = viewport_size.y
	ground_sprite.scale.x = viewport_size.x / 800
	hud.set_score(0)
	
	setup_parallax_layer(parallazx1)
	setup_parallax_layer(parallazx2)
	setup_parallax_layer(parallazx3)
	hud.pause_game.connect(_on_hud_pause_game)
	#new_game()
	ground_sprite.visible = false
	
	load_score()

func get_parallax_sprite_scale(parallax_sprite: Sprite2D):
	var parallax_texure = parallax_sprite.get_texture()
	var paralax_texture_width = parallax_texure.get_width()
	
	var _scale = viewport_size.x / paralax_texture_width
	var result = Vector2(_scale,_scale)
	return result

func  setup_parallax_layer(parallax_layer: ParallaxLayer):
	var parallax_sprite = parallax_layer.find_child("Sprite2D")
	if parallax_sprite != null:
		parallax_sprite.scale = get_parallax_sprite_scale(parallax_sprite)
		var my = parallax_sprite.scale.y * parallax_sprite.get_texture().get_height()
		parallax_layer.motion_mirroring.y = my
		

func _process(_delta: float):
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()
	if Input.is_action_just_pressed("reset"):
		get_tree().reload_current_scene()
		#if FileAccess.file_exists(save_file_path):
			#high_score = 0
			#var file = FileAccess.open(save_file_path, FileAccess.WRITE)
			#file.store_var(high_score)
	if player:
		if score < (viewport_size.y - player.global_position.y):
			@warning_ignore("narrowing_conversion")
			score = (viewport_size.y - player.global_position.y)
			hud.set_score(score)
	
	
func  new_game():
	reset_game()
	await get_tree().create_timer(0.1).timeout
	player = player_scene.instantiate()
	player.global_position = player_spown_pos
	add_child(player)
	if new_skin_unlocked:
		player.use_new_skin()
	camera = camera_scene.instantiate()
	camera.setup_camera($Player)
	add_child(camera)
	player.died.connect(_on_player_died)
	
	if player:
		level_generator.setup(player)
		level_generator.start_generation()
	
	hud.visible = true
	ground_sprite.visible = true
	score = 0

func  _on_player_died():
	hud.visible = false
	if score > high_score:
		high_score = score
		save_score()
		
	player_died.emit(score, high_score)

func reset_game():
	ground_sprite.visible = false
	hud.visible = false
	hud.set_score(0)
	level_generator.reset_level()
	if player != null:
		player.queue_free()
		player = null
		level_generator.player = null
	if camera != null:
		camera.queue_free()
		camera = null

func save_score():
	var file = FileAccess.open(save_file_path, FileAccess.WRITE)
	file.store_var(high_score)
	file.close()
func load_score():
	if FileAccess.file_exists(save_file_path):
		var file = FileAccess.open(save_file_path, FileAccess.READ)
		high_score = file.get_var()
		file.close()
	else:
		high_score = 0
		
func _on_hud_pause_game():
	pause_game.emit()
