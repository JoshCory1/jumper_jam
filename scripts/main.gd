extends Node


@onready var game = $Game
@onready var screens = $Screens
@onready var iap_manager = $IAPManager

func _ready() -> void:
	game.pause_game.connect(_on_game_pause_game)
	game.player_died.connect(_on_game_player_died)
	screens.start_game.connect(_on_screens_start_game)
	screens.delete_level.connect(_on_screens_delete_level)
	
	#IAP signals
	iap_manager.unlock_new_skin.connect(_iap_manager_unlock_new_skin)
	screens.purchase_skin.connect(_onScreens_purchase_skin) 

func _on_screens_start_game():
	game.new_game()

func _on_game_player_died(score, highscore):
	screens.game_over(score, highscore)

func _on_screens_delete_level():
	game.reset_game()


func _on_game_pause_game():
	get_tree().paused = true
	screens.pause_gmae()

# IAP Signals

func _onScreens_purchase_skin():
	if game.new_skin_unlocked == false:
		game.new_skin_unlocked = true
		print("Unlocking new skin")

func  _iap_manager_unlock_new_skin():
	iap_manager.purchase_skin()
