extends Control

@onready var top_bar = $TopBar
@onready var top_bar_bg = $TopBarBG
@onready var score_lable = $TopBar/ScoreLable
@onready var play_click = $AudioStreamPlayer

signal pause_game

func _ready():
	var os_name = OS.get_name()
	if os_name == "Android" || os_name == "iOS":
		var safe_area = DisplayServer.get_display_safe_area()
		var safe_area_top = safe_area.position.y
		
		if os_name == "iOS":
			var screen_scale = DisplayServer.screen_get_scale()
			safe_area_top = (safe_area_top / screen_scale)
			MyUtility.add_log_msg("Screen scale: " + str(screen_scale))
		if os_name == "Android":
			var screen_scale = DisplayServer.screen_get_scale()
			safe_area_top = (safe_area_top / screen_scale / 3)
		top_bar.position.y += safe_area_top
		var margin = 10
		top_bar_bg.size.y += safe_area_top + margin
		
		
		MyUtility.add_log_msg("Safe area: " + str(safe_area))
		MyUtility.add_log_msg("Window size: " + str(DisplayServer.window_get_size()))
		MyUtility.add_log_msg("Safe area top: " + str(safe_area_top))
		MyUtility.add_log_msg("top bar posistion y: " + str(top_bar.position.y))
		MyUtility.add_log_msg("top bar bd size y: " + str(top_bar_bg.size.y))
		

func _on_pasue_button_pressed():
	play_click.play()
	pause_game.emit()

func set_score(new_score):
	score_lable.text = str(new_score)
