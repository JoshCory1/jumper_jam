extends CharacterBody2D
class_name Player

@export var speed: float = 300.0
@export var slide: float = 40.0
@export var jump_velocity: float = -800

var fall_anim_name: String = "Fall"
var jump_anim_name: String = "Jump"
var accelerometer_speed = 130.0
var gravity: float = 15.0
var max_fall_velocity: float = 1000.0
var viewport_size
var use_accelerometer: bool = false
var dead: bool = false
@onready var player_jump = $AudioStreamPlayer_1
@onready var player_fall = $AudioStreamPlayer_2
@onready var animator = $AnimationPlayer
@onready var cshape = $CollisionShape2D
@onready var sprite = $Sprite2D


signal died

func _ready():
	viewport_size = get_viewport_rect().size
	
	var os_name = OS.get_name()
	if os_name == "Android" || os_name == "iOS":
		use_accelerometer = true


func _process(_delta: float):
	if velocity.y > 0:
		if animator.current_animation != fall_anim_name:
			animator.play(fall_anim_name)
	if velocity.y < 0:
		if animator.current_animation != jump_anim_name:
			animator.play(jump_anim_name)


func _physics_process(_delta: float):
	velocity.y += gravity
	if velocity.y > max_fall_velocity:
		velocity.y = max_fall_velocity
	if !dead:
		if use_accelerometer == true:
			var mobile_input = Input.get_accelerometer()
			velocity.x = mobile_input.x * accelerometer_speed
		
		else:
			var direction = Input.get_axis("move_left","move_right")
			if direction:
				velocity.x = direction * speed
			else:
				velocity.x = move_toward(velocity.x, 0, speed / slide)

	move_and_slide()
	
	var margin = 20
	if global_position.x > viewport_size.x + margin:
		global_position.x = -margin
	if global_position.x < -margin:
		global_position.x = viewport_size.x + margin

func jump():
	player_jump.play()
	velocity.y = jump_velocity


func _on_visible_on_screen_notifier_2d_screen_exited():
	die()

func die():
	if !dead:
		player_fall.play()
		sprite.visible = false
		dead = true
		cshape.set_deferred("disabled", true)
		died.emit()

func use_new_skin():
	fall_anim_name = "fall_red"
	jump_anim_name = "jump_red"
	
	if sprite:
		sprite.texture = preload("res://assets/godot_mobile_assets/textures/character/Skin2Idle.png")
