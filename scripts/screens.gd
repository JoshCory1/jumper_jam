extends CanvasLayer
@onready var shop_screen = $ShopScreen
@onready var console = $Debug/ConsoleLog
@onready var title_screen = $TitleScreen
@onready var pause_screen = $PauseScreen
@onready var game_over_screen = $GameOverScreen
@onready var main = $".."
@onready var game_over_score_lable = $GameOverScreen/Box/ScoreLabel
@onready var game_over_highscore_lable = $GameOverScreen/Box/ScoreLabel2
@onready var player_click = $AudioStreamPlayer

var current_screen = null

signal purchase_skin
signal start_game
signal delete_level

# Called when the node enters the scene tree for the first time.
func _ready():
	console.visible = false
	register_buttons()
	change_screen(title_screen)

func register_buttons():
	var buttons = get_tree().get_nodes_in_group("buttons")
	if buttons.size() > 0:
		for button in buttons:
			if button is ScreenButton:
				button.clicked.connect(_on_button_pressed)

func _on_button_pressed(button):
	player_click.play()
	match button.name:
		"TitlePlay":
			change_screen(null)
			await(get_tree().create_timer(0.5).timeout)
			start_game.emit()
	match  button.name:
		"PauseRetry":
			change_screen(null)
			await(get_tree().create_timer(0.75).timeout)
			get_tree().paused = false
			start_game.emit()
		"PauseBack":
			change_screen(title_screen)
			get_tree().paused = false
			delete_level.emit()
		"PauseClose":
			change_screen(null)
			await(get_tree().create_timer(0.75).timeout)
			get_tree().paused = false
		"GameOverRetry":
			change_screen(null)
			await (get_tree().create_timer(0.5).timeout)
			start_game.emit()
		"GameOverBack":
			change_screen(title_screen)
			delete_level.emit()
		"TitleShop":
			change_screen(shop_screen)
		"ShopBack":
			change_screen(title_screen)
		"ShopPurchaseSkin":
			purchase_skin.emit()
			change_screen(title_screen)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float):
	pass


func _on_toggle_console_pressed():
	console.visible = ! console.visible

func change_screen(new_screen):
	if current_screen:
		var disappear_tween = current_screen.disappear()
		await(disappear_tween.finished)
		current_screen.visible = false
	current_screen = new_screen
	if current_screen:
		var appear_tween = current_screen.appear()
		await(appear_tween.finished)
		get_tree().call_group("buttons", "set_disabled", false)

func game_over(score, highscore):
	game_over_score_lable.text = "Score: " + str(score)
	game_over_highscore_lable.text = "Best: " + str(highscore)
	await(get_tree().create_timer(1.0).timeout)
	change_screen(game_over_screen)

func pause_gmae():
	change_screen(pause_screen)
